import { resolveCompany, resolveLocation, resolveProfile } from './api.js'
import render from './render.js'

(async () => {
    const id = '1'

    const user = await fetch(resolveProfile(id));
    const userData = await user.json();

    const company = await fetch(resolveCompany(userData.companyId));
    const companyData = await company.json();

    const location = await fetch(resolveLocation(companyData.locationId))
    const locationData = await location.json();

    render({
        user: userData,
        location: locationData,
        company: companyData,
    })
})();