import {resolveCompany, resolveLocation, resolveProfile} from './api.js'
import render from './render.js'

function loadData() {
    const data = {
        user:null,
        company:null,
        location:null,
    }

    const queue = [
        () =>{
            return fetch(resolveProfile('3'))
        },
        userData =>{
            data.user = userData
            return fetch(resolveCompany(userData.companyId))
        },
        userCompany =>{
            data.company = userCompany;

            return fetch(resolveLocation(userCompany.locationId))
        },
        companyLocatoin =>{
            data.location = companyLocatoin

            render(data)
        }
    ];

    function execQueue(acc, next){
        return acc.then(res =>{
            if(res && 'json' in res){
                return res.json();
            }else{
                return res;
            }
        }).then(next)
    }

    queue.reduce(execQueue, Promise.resolve());

}

loadData()